﻿using Spender.Models;

namespace Spender.Repository
{
    public interface IPaymentModeRepository : IBaseRepository<PaymentMode>
    {
    }
}