﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Spender.DAL;
using Spender.Models;

namespace Spender.Repository
{
    public class PaymentModeRepository : BaseRepository<PaymentMode>, IPaymentModeRepository
    {
        public PaymentModeRepository(SpenderContext context): base(context)
        {
        }

        //public override PaymentMode GetById(int id)
        //{
        //    return this.dbSet.AsNoTracking().SingleOrDefault(x => x.PaymentModeId == id);
        //}

        public override void Save(PaymentMode item)
        {
            if (item.PaymentModeId > 0)
            {
                var originalItem = this.dbSet.AsNoTracking().SingleOrDefault(x => x.PaymentModeId == item.PaymentModeId);
                if (originalItem != null)
                {
                    this.dbSet.Attach(item);
                    this.context.Entry<PaymentMode>(item).State = EntityState.Modified;
                }
            }
            else
            {
                this.dbSet.Add(item);
            }

            this.context.SaveChanges();
        }
    }
}