﻿using System.Collections.Generic;
using Spender.DAL;
using Spender.Models;

namespace Spender.Repository
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        IEnumerable<Category> GetParents();
        IEnumerable<Category> GetChildren(int parentId);
        void Delete(int categoryId);
    }
}