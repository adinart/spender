﻿using System.Collections.Generic;
using System.Linq;
using Spender.DAL;
using Spender.Models;
using System.Data.Entity;

namespace Spender.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(SpenderContext context) : base(context)
        {
        }

        public IEnumerable<Category> GetParents()
        {
            return base.dbSet.Where(x => x.ParentId == null);
        }

        public IEnumerable<Category> GetChildren (int parentId)
        {
            var query = from c in dbSet
                        where c.CategoryId == parentId
                        select c;

            return base.dbSet.Where(x => x.ParentId == parentId);
        }

        public override void Save(Category item)
        {
            if (item.CategoryId > 0)
            {
                var originalItem = this.dbSet.AsNoTracking().SingleOrDefault(x => x.CategoryId == item.CategoryId);
                if (originalItem != null)
                {
                    this.dbSet.Attach(item);
                    this.context.Entry<Category>(item).State = EntityState.Modified;
                }
            }
            else
            {
                this.dbSet.Add(item);
            }

            this.context.SaveChanges();
        }

        public void Delete(int categoryId)
        {
            if (base.TryDelete(categoryId))
            {
                base.Commit();
            }
        }
    }
}