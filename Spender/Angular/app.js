﻿'use strict';

angular.module('spenderApp.service', ['ngResource']);

var app = angular.module('spenderApp', ['ngRoute', 'spenderApp.service', 'ui.bootstrap']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/', { redirectTo: '/category/list' })
    .when('/category', { redirectTo: '/category/list' })
    .when('/category/list' , { templateUrl: '/template/category/list', controller: 'CategoryListController', caseInsensitiveMatch: true })
    .when('/category/edit/:categoryId', { templateUrl: '/template/category/edit', controller: 'CategoryEditController', caseInsensitiveMatch: true })
    .otherwise({ redirectTo: '/' });
}]);

// app.run

