﻿'use strict';

var service = angular.module('spenderApp.service');

service.factory('CategoryService', ['$resource',
    function ($resource) {
        return $resource('/api/category/:categoryId', { categoryId: '@categoryId' }, 
            {
                // query does not need to be defined
                getChildren: {
                    method: 'GET',
                    url : '/api/category/getChildren/:parentId',
                    params: { parentId: '@parentId' }, 
                    isArray: true
                },
                save: { method: 'POST', url: '/api/category', isArray: false }
            });
    }]);
