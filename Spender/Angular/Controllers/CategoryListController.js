﻿'use strict';

var app = angular.module('spenderApp');

app.controller('CategoryListController', ['$scope', '$route', '$routeParams', '$location', 'CategoryService', function ($scope, $route, $routeParams, $location, CategoryService) {

    $scope.pageTitle = 'Category List';
    $scope.messages = [];
    $scope.categories = CategoryService.query();

    $scope.deleteCategory = function (categoryId) {
        // TODO: confirmation
        $scope.messages.length = 0;
        CategoryService.delete({ categoryId: categoryId },
            function (success) {
                $scope.categories = CategoryService.query();
            },
            function (error) {
                $scope.messages.push({type: 'alert', message: error.status + ' - ' + error.statusText});
            });
    };

    $scope.toggleShowChildren = function (parent) {
        var children = CategoryService.getChildren({ parentId: parent.categoryId }, function () {
            if (children) {
                parent.hasChildren = true;
                parent.children = CategoryService.getChildren({ parentId: parent.categoryId });
            }
            else {
                parent.hasChildren = false;
                parent.children = null;
            }

            parent.showChildren = !parent.showChildren;
        });
    };

}]);