﻿'use strict';

var app = angular.module('spenderApp');

app.controller('CategoryEditController', ['$scope', '$route', '$routeParams', '$location', 'CategoryService', function ($scope, $route, $routeParams, $location, CategoryService) {

    $scope.pageTitle = 'Edit Category';
    $scope.categoryId = $routeParams.categoryId;    

    $scope.category = CategoryService.get({ categoryId: $scope.categoryId }, function (success) {
        // edit
        if ($scope.categoryId > 0) {
            if ($scope.category.parentId > 0) {
                $scope.hasParent = true;
                $scope.parent = $scope.category.parent;                
            }
            else {
                $scope.hasParent = false;
            };
        }
        // add
        else {
            $scope.hasParent = false;
        }

        $scope.parentCategories = CategoryService.query();
    }, function (error) { });

    $scope.toggleHasParent = function () {
        if (!$scope.hasParent) {
            $scope.parent = null;
        }
    }

    $scope.selectedParentChanged = function (item) {
        $scope.category.parentId = item.categoryId;
        $scope.parent = item;
    }

    $scope.save = function () {
        if ($scope.parent) {
            $scope.category.parentId = $scope.parent.categoryId;
        }
        else {
            $scope.category.parentId = null;
        }
        $scope.category.$save(function (success) {
            $location.path('/category/list');
        }, function (error) {
            alert('error');
            // display error notif
        });
    }

}]);