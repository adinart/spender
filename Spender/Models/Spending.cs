﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spender.Models
{
    public class Spending
    {
        public int SpendingId { get; set; }
        public int CategoryId { get; set; }
        public int PaymentModeId { get; set; }
        public double Amount { get; set; }
        public DateTime TheDate { get; set; }
        public string Note { get; set; }

        public virtual Category Category { get; set; }
        public virtual PaymentMode PaymentMode { get; set; }
    }
}