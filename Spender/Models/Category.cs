﻿using System.Collections.Generic;

namespace Spender.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public bool IsSummaryHeader { get; set; }

        public virtual ICollection<Spending> Spendings { get; set; }
        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> Children { get; set; }
    }
}