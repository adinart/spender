﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Spender.Models
{
    public class PaymentMode
    {        
        public int PaymentModeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Spending> Spendings { get; set; }
    }
}