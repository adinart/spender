﻿using Autofac;
using Spender.DAL;
using Spender.Models;
using Spender.Repository;
using System.Data.Entity;

namespace Spender
{
    public class RegisterModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<SpenderContext>(t => { return new SpenderContext(); });

            builder.RegisterType<PaymentModeRepository>().As<IPaymentModeRepository>();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();
        }
    }
}