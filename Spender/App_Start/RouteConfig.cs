﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Spender
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
            name: "Account",
            url: "account/{action}/{id}",
            defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional },
            namespaces: new[] { "Spender.Controllers" });

            routes.MapRoute(
            name: "Category",
            url: "category/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "Spender.Controllers" });

            routes.MapRoute(
            name: "SpenderCategory",
            url: "spender/category/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "Spender.Controllers" });

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "Spender.Controllers" });

            // map all url to Home/Index
            //routes.MapRoute(
            //name: "Other",
            //url: "{a}/{b}/{id}",
            //defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //namespaces: new[] { "Spender.Controllers" });
        }
    }
}