﻿using AutoMapper;
using Spender.Models;
using Spender.ViewModels;

namespace Spender
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<PaymentMode, PaymentModeViewModel>();
                cfg.CreateMap<PaymentModeViewModel, PaymentMode>().ForMember(x => x.Spendings, opt => opt.Ignore());

                cfg.CreateMap<Category, CategoryViewModel>()
                    .ForMember(x => x.ChildrenCount, opt => opt.MapFrom(source => source.Children.Count));
                cfg.CreateMap<CategoryViewModel, Category>()
                    .ForMember(x => x.Spendings, opt => opt.Ignore())
                    .ForMember(x => x.Parent, opt => opt.Ignore())
                    .ForMember(x => x.Children, opt => opt.Ignore());

                cfg.CreateMap<Spending, SpendingViewModel>();
                cfg.CreateMap<SpendingViewModel, Spending>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}