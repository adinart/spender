﻿namespace Spender.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public bool IsSummaryHeader { get; set; }
        public CategoryViewModel Parent { get; set; }
        public int ChildrenCount { get; set; }
    }
}