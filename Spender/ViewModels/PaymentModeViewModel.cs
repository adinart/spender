﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Spender.ViewModels
{
    public class PaymentModeViewModel
    {
        [DisplayName("Id")]
        public int PaymentModeId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}