﻿using System.Collections.Generic;

namespace Spender.ViewModels
{
    public class PaymentModeIndex
    {
        public string PageTitle { get; set; }
        public IEnumerable<PaymentModeViewModel> PaymentModes { get; set; }
        public PaymentModeViewModel PaymentMode { get; set; }
    }
}