﻿using System.Data.Entity;
using Spender.Models;

namespace Spender.DAL
{
    public interface IUnitOfWork
    {
        IDbSet<PaymentMode> PaymentModes { get; }
        IDbSet<Category> Categories { get; }
        IDbSet<Spending> Spendings { get; }        
    }
}
