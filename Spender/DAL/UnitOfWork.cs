﻿using System.Data.Entity;
using Spender.Models;

namespace Spender.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private SpenderContext db;

        public UnitOfWork(SpenderContext db)
        {
            this.db = db;
        }

        public IDbSet<PaymentMode> PaymentModes { get { return db.PaymentModes; } }
        public IDbSet<Category> Categories { get { return db.Categories; } }
        public IDbSet<Spending> Spendings { get { return db.Spendings; } }
    }
}