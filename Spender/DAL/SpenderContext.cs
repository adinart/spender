﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Spender.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;

namespace Spender.DAL
{
    public class SpenderContext : IdentityDbContext<ApplicationUser>
    //public class SpenderContext : DbContext
    {
        public SpenderContext()
            : base("SpenderDB", throwIfV1Schema: false) 
        {
            //Configuration.ProxyCreationEnabled = false;
        }

        // called by Owin, TODO: see if can be injected using DI instead
        public static SpenderContext Create()
        {
            return new SpenderContext();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<PaymentMode> PaymentModes { get; set; }
        public DbSet<Spending> Spendings { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Category>().Property(c => c.CategoryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // self-referencing FK in Category
            modelBuilder.Entity<Category>()
                .HasOptional(c => c.Parent)
                .WithMany(c => c.Children)
                .HasForeignKey(c => c.ParentId);

            base.OnModelCreating(modelBuilder);            
        }
    }
}