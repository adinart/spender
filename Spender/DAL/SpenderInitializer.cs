﻿using Spender.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace Spender.DAL
{
    public class SpenderInitializer: DropCreateDatabaseIfModelChanges<SpenderContext>
    {
        protected override void Seed(SpenderContext context)
        {
            List<PaymentMode> modes = new List<PaymentMode>();
            modes.Add(new PaymentMode() { Name = "Cash" });
            modes.Add(new PaymentMode() { Name = "Halifax card" });
            modes.ForEach(x => context.PaymentModes.Add(x));
            context.SaveChanges();

            int? topParent = null;
            bool parentSummary = true, childSummary = false;
            int parentId;
            int categoryId = 1;

            List<Category> categories = new List<Category>();
            categories.Add(new Category() { CategoryId = categoryId, ParentId = topParent, Name = "Accomodation", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Mortgage", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Rent & Service Charge", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Bills", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Council Tax", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Gas", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Water", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Heating interchange", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Sky broadband", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Groceries", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Food", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Toilettries", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Eating out - cash", IsSummaryHeader = parentSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Eating out - card", IsSummaryHeader = parentSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Transport", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Oyster", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Train", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Contactless", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Telephone", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Giff Gaff", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Skype", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Well-being", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Chiro", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Massage", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Gym membership", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Leisure", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Rdio Subscription", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Etc", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Professional", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Books", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Subscription", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Etc", IsSummaryHeader = childSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Misc", IsSummaryHeader = parentSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Gift/Present", IsSummaryHeader = parentSummary });

            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = topParent, Name = "Trip", IsSummaryHeader = parentSummary });
            parentId = categoryId;
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Trip - transport", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Trip - hotel", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Trip - food", IsSummaryHeader = childSummary });
            categories.Add(new Category() { CategoryId = ++categoryId, ParentId = parentId, Name = "Trip - etc", IsSummaryHeader = childSummary });


            categories.ForEach(x => context.Categories.Add(x));
            context.SaveChanges();
        }
    }
}