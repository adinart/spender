﻿using Spender.Models;
using Spender.Repository;
using Spender.ViewModels;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;

namespace Spender.Controllers
{
    public class PaymentModeController : Controller
    {
        private IPaymentModeRepository paymentModeRepository;

        public PaymentModeController(IPaymentModeRepository paymentModeRepository)
        {
            this.paymentModeRepository = paymentModeRepository;
        }

        [Route("PaymentMode/Index")]
        public ActionResult Index()
        {
            var vm = new PaymentModeIndex();
            vm.PageTitle = "Payment Mode";
            vm.PaymentMode = new PaymentModeViewModel();
            return View(vm);
        }

        [Route("PaymentMode/List")]
        public ActionResult List()
        {
            IEnumerable<PaymentModeViewModel> vm = Mapper.Map<IEnumerable<PaymentModeViewModel>>(this.paymentModeRepository.Get());
            return PartialView(vm);
        }

        [Route("PaymentMode/Edit")]
        [HttpGet]
        public ActionResult Edit(int paymentModeId)
        {
            var paymentMode = this.paymentModeRepository.GetById(paymentModeId);
            var model = Mapper.Map<PaymentModeViewModel>(paymentMode);
            return PartialView("Edit", model);
        }

        [Route("PaymentMode/Edit")]
        [HttpPost]
        public ActionResult Edit(PaymentModeViewModel model)
        {
            PaymentMode paymentMode = Mapper.Map<PaymentMode>(model);
            this.paymentModeRepository.Save(paymentMode);
            return RedirectToAction("Index");
        }
    }
}