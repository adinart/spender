﻿using AutoMapper;
using Spender.Models;
using Spender.Repository;
using Spender.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Spender.Controllers.api
{
    public class CategoryController : ApiController
    {
        private ICategoryRepository categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        [Route("api/Category")]
        public IEnumerable<CategoryViewModel> Get()
        {
            return Mapper.Map<IEnumerable<CategoryViewModel>>(this.categoryRepository.GetParents());
        }

        [Route("api/Category/{categoryId}")]
        public CategoryViewModel Get(int categoryId)
        {
            if (categoryId == 0)
            {
                return new CategoryViewModel();
            }
            else
            {
                Category category = this.categoryRepository.GetById(categoryId);
                return Mapper.Map<CategoryViewModel>(category);
            }
        }

        [Route("api/Category/GetChildren/{parentId}")]
        public IEnumerable<CategoryViewModel> GetChildren(int parentId)
        {
            return Mapper.Map<IEnumerable<CategoryViewModel>>(this.categoryRepository.GetChildren(parentId));
        }

        [Route("api/Category")]
        public void Post(CategoryViewModel model)
        {
            Category category = Mapper.Map<Category>(model);
            this.categoryRepository.Save(category);
            //throw new Exception("error!!!");
        }

        [Route("api/Category/{categoryId}")]
        public void Delete(int categoryId)
        {
            this.categoryRepository.Delete(categoryId);
        }
    }
}
