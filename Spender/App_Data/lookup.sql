declare @topParent int
declare @summary bit
declare @parentId int
set @topParent = null
set @summary = 1

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'accomodation',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'mortgage')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'rent & service charge')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'bills',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'council tax')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'gas')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'water ')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'heating interchange')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'sky')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'groceries',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'food')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'toilettries')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'eating out - cash',@summary)
INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'eating out - card',@summary)
INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'transport',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'oyster')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'train')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'contactles')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'telephone',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'giff gaff')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'skype')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'well-being',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'chiro')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'massage')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'gym membership')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'leisure',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'rdio subsription')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'etc')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'professional ',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'books')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'subsription')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'etc')

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'misc',@summary)

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'gift/present',@summary)

INSERT INTO Category (ParentId,Name,IsSummaryHeader) VALUES (@topParent,'trip',@summary)
set @parentId = @@IDENTITY
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'trip - transport')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'trip - hotel')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'trip - food')
INSERT INTO Category (ParentId,Name) VALUES (@parentId,'trip - etc')


INSERT INTO PaymentMode (Name) Values ('cash')
INSERT INTO PaymentMode (Name) Values ('halifax card')