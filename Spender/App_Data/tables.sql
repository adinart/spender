

CREATE TABLE Category(
	CategoryId INT IDENTITY(1,1) NOT NULL,
	ParentId INT NULL DEFAULT(0),
	Name [NVARCHAR](50) NOT NULL,
	IsSummaryHeader BIT NOT NULL DEFAULT(0),
	CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED(CategoryId ASC),
	CONSTRAINT [FK_Category_Parent] FOREIGN KEY (ParentId) References Category(CategoryId)
)
GO

CREATE TABLE PaymentMode(
	PaymentModeId INT IDENTITY(1,1) NOT NULL,
	Name [NVARCHAR](50) NOT NULL,
	CONSTRAINT [PK_PaymentMode] PRIMARY KEY CLUSTERED(PaymentModeId ASC)
)

GO

CREATE TABLE Spending(
	SpendingId INT IDENTITY(1,1) NOT NULL,
	CategoryId INT NOT NULL,
	TheDate DATETIME NOT NULL,
	Amount FLOAT NOT NULL DEFAULT (0.0),
	PaymentModeId INT NOT NULL DEFAULT(0),
	Note [NVARCHAR](100) NULL,
	CONSTRAINT [PK_Amount] PRIMARY KEY CLUSTERED(SpendingId ASC),
	CONSTRAINT [FK_Category] FOREIGN KEY (CategoryId) REFERENCES Category(CategoryId),
	CONSTRAINT [FK_PaymentMode] FOREIGN KEY (PaymentModeId) REFERENCES PaymentMode(PaymentModeId)
)

